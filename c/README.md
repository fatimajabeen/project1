# Files

1. matrix.c
   - You should write your code in this file.
2. matrix.h
   - This is a header file - it contains the interface to matrix (set of related
     functions)
2. matrix_test.c
   - This file contains tests which will test your code. Feel free to add extra
     test cases to cover edge cases we didn't.

# The Problem

Given a string which represents a matrix of numbers, use the template matrix
constructor in [matrix.c](matrix.c) and fill in the two functions which return
the rows and columns of that matrix as arrays.

# Building the Source

You can either use `gcc` directly to build main.c (i.e. `gcc matrix.c -o matrix`)
or use the [Makefile](https://www.gnu.org/software/make/manual/make.html):
```
  make
```
Note that the main function currently resides in the tests, if you want to make
a basic command line application that consumes a stringified matrix and prints
it, that might be a nice stretch goal. You may want to separate the tests and src
files to make the resulting Makefile cleaner.

# Running the tests

To run the tests, there is a `test` target in the provided Makefile:
```
  make test
```
