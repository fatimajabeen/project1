#include <stdio.h>

#include "matrix.h"

#define MATRIX_MAX_LEN (MATRIX_MAX_HEIGHT * MATRIX_MAX_WIDTH)


static bool is_decimal(char ch)
{
	return ((ch <= '9') && (ch >= '0'));
}


matrix_t matrix_create(const char* matrix_string)
{
	if (!matrix_string)
		return matrix_empty;

	matrix_t matrix;

	// fill me in

	return matrix;
}


void matrix_print(const matrix_t* matrix)
{
	// fill me in
}


bool matrix_equal(const matrix_t* a, const matrix_t* b)
{
	// fill me in
}


size_t matrix_row(const matrix_t* matrix, unsigned index,
                  int32_t* values, size_t len)
{
	if (!matrix || (index >= matrix->height)
	    || !values || (len < matrix->width))
		return 0;

	// fill me in
}


size_t matrix_col(const matrix_t* matrix, unsigned index,
                  int32_t* values, size_t len)
{
	if (!matrix || (index >= matrix->width)
	    || !values || (len < matrix->height))
		return 0;

	// fill me in
}
