# A class to represent a Matrix
class Matrix(object):
	def __init__(self, matrix_string):
		self.matrix_string= matrix_string
        
	def to_list(self):
		matrix_list = self.matrix_string.split('\n')
		main_list=[]
		for m in matrix_list:
			mini=m.split(' ')
			mini=list(map(int,mini))
			main_list.append(mini)			
		return main_list

	def row(self, index):	
		main_list=self.to_list()
		return main_list[index]

	def column(self, index):
		main_list=self.to_list()
		return [row[index] for row in main_list]
        
        
    
    
