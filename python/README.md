# Files

1. matrix.py
  - You should write your code in this file.
2. matrix_test.py
  - This file contains tests which will test your code. Feel free to add extra
    test cases to cover edge cases we didn't.

# The Problem

Use the template matrix class in [matrix.py](matrix.py) to write two functions
which return the rows and columns of that matrix.

For the example - "9 8 7\n5 3 2\n6 6 7" Matrix.row(1) should return `[5, 3, 2]`.


# Running the tests
In order to test our [matrix class](matrix.py) works as expected, tests have been
written that use the [pytest framework](https://docs.pytest.org/en/latest/).

To run the tests, run the appropriate command below:

    pytest matrix_test.py

If this does not work, you probably don't have pytest installed as it's own package.
So, alternatively, you can try to Python to run the pytest module:

    python3 -m pytest matrix_test.py

If neither of these commands work, you'll have to install Pytest on your system.
If you're using debian, try:

    sudo apt install python3-pytest

To verify this has installed, try `pytest --help`, and you should see the Pytest
help message.

Consult your supervisor if you need help or once you have all the tests passing.


## Common `pytest` options

- `-v` : enable verbose output
- `-x` : stop running tests on first failure
- `--ff` : run failures from previous test before running other test cases

For other options, see `python -m pytest -h`